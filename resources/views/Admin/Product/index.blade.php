

@component('components.panel_default_with_blank')
    @slot('active') Product @endslot
    @slot('page_title') Products @endslot
    @slot('panel_title') Products @endslot

    @slot('body')

        <div id="myVue">
          <button class="btn btn-primary btn-rounded" v-on:click="showCreateModel()">
                create <i class="fa fa-plus mydir"></i>
          </button>
          <br><br>
          <!--.......START Search area.........--->
          {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent'=>'getResults()']) !!}
             <div class="col-md-6  ">
                <label> search </label>
                <input type="text" name="search" class="form-control mydirection"  placeholder=" search"  >
             </div>
             <div class="col-md-6">
               <label> order </label>
               <v-select :options="order_options" :name="'order'" v-on:s_change="getResults()" :f_item="'choose'"></v-select>
            </div>
         {!! form::close() !!}
         <!--.......END Search area.........--->
          <br><br><br>

        <!--.......START Content.........--->
            <table class="table mydir">
                <thead>
                    <th>  id </th>
                    <th>  image  </th>
                    <th>  name </th>
                    <th> Quanity  </th>
                    <th> Price  </th>
                    <th> create date  </th>
                    <th> update date  </th>
                    <th>  more </th>
                </thead>
                <tbody>
                  <tr v-for="(list,index) in mainList.data">
                      <td> <p v-text="list.id"></p>  </td>
                      <td> <img :src="list.image" width="120px" class="img-thumbnail"> </td>
                      <td> <p v-text="list.name"></p>  </td>
                      <td> <p v-text="list.Quanity"></p>  </td>
                      <td> <p v-text="list.Price"></p>  </td>
                      <td> <p> @{{list.created_at}} </p> <p> [@{{diffforhumans(list.created_at)}}] </p>  </td>
                      <td> <p> @{{list.updated_at}} </p> <p> [@{{diffforhumans(list.updated_at)}}] </p>  </td>
                      <td>
                           <button class="btn btn-primary btn-rounded" v-on:click="showEditModel(list)" >
                              <i class="fa fa-pencil"></i>
                           </button>
                           <button type="button" class="btn btn-danger btn-rounded" v-on:click="DeleteMessage(list.id,index)" >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>
            <!--.......END Content.........--->

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->

            {{-- ------------------- create modal------------------------ --}}

                @component('components.modal')
                    @slot('id')
                      create_model
                    @endslot
                    @slot('header')
                           add new
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'id'=>'create_form' ,'v-on:submit.prevent'=>'create()' ]) !!}
                    @endslot
                    @slot('body')

                      <div class="form-group">
                          {!! Form::label('image','image') !!}
                          {!! Form::file('image',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'create' ,'id'=>'create_image','required' ,'data-from'=>'create']) !!}
                      </div>
                      <img id="Preview_image_create" width="200px" style="min-height:100px" class="btn btn-danger btn-rounded">

                      <div class="form-group">
                          {!! Form::label('name','name') !!}
                          {!! Form::text('name',null,['class'=>'form-control','required' ]) !!}
                      </div>
                      <div class="form-group">
                          {!! Form::label('Quanity','Quanity') !!}
                          {!! Form::number('Quanity',null,['class'=>'form-control','required' ]) !!}
                      </div>
                      <div class="form-group">
                          {!! Form::label('Price','Price') !!}
                          {!! Form::number('Price',null,['class'=>'form-control','required' ]) !!}
                      </div>

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" >  Add new </button>
                    @endslot
                @endcomponent


            {{-- ------------------- edit modal------------------------ --}}

                @component('components.modal')
                    @slot('id')
                      edit_model
                    @endslot
                    @slot('header')
                          edit
                    @endslot
                    @slot('form_header')
                        {!! Form::open(['url'=>'Product/update', 'id'=>'edit_form' ,'v-on:submit.prevent'=>'edit()'  ]) !!}
                    @endslot
                    @slot('body')

                      {!! Form::hidden('id',null,['id'=>'edit_id','v-model'=>'EF.id']) !!}

                      <div class="form-group">
                          {!! Form::label('image','image') !!}
                          {!! Form::file('image',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'edit','id'=>'edit_image','data-from'=>'edit' ]) !!}
                      </div>
                      <img :src="EF.image" id="Preview_image_edit" width="200px" style="min-height:100px" class="btn btn-danger btn-rounded">

                      <div class="form-group">
                          {!! Form::label('name','name' ) !!}
                          {!! Form::text('name',null,['class'=>'form-control','required' ,'v-model'=>'EF.name']) !!}
                      </div>
                      <div class="form-group">
                          {!! Form::label('Quanity','Quanity') !!}
                          {!! Form::number('Quanity',null,['class'=>'form-control','required' ,'v-model'=>'EF.Quanity']) !!}
                      </div>
                      <div class="form-group">
                          {!! Form::label('Price','Price') !!}
                          {!! Form::number('Price',null,['class'=>'form-control','required' ,'v-model'=>'EF.Price']) !!}
                      </div>

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" > edit </button>
                    @endslot
                @endcomponent


       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api = '{{url('Product/delete')}}';
            var get_list = '{{url('Product/list')}}';
            var create_api = '{{url('Product/create')}}';
            var update_api = '{{url('Product/update')}}';
        </script>
        <script src="{{asset('js/Product.js')}}"> </script>
    @endslot

@endcomponent
