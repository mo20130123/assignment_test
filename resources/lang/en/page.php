<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Category' => 'Category',
    'Category' => 'Category',
    'create' => 'create',
    'name_en' => 'name English',
    'name_ar' => 'name Arabic',
    'name' => 'name',
    'more' => 'more',
    'status' => 'status',
    'code' => 'code',
    'logo' => 'logo',
    'Location' => 'Location',
    'City' => 'City',
    'Governorate' => 'Governorate',
    'Company' => 'Company',
    'users' => 'users',
    'type' => 'type',
    'full_location' => 'location',
    'street' => 'street',
    'building_no' => 'building no',
    'code' => 'code',
    'zip_code' => 'zip code',
    'phone' => 'phone',
    'CommercialRegistrationNo' => 'Commercial Registration No',
    'CommercialRegistrationType' => 'Commercial Registration Type',
    'Activity' => 'Activity',
    'employees_no' => 'employees no',
    'accaptance_from_admin' => 'accaptance from admin',
    'Buyer' => 'Buyer',
    'Buyer' => 'Buyer',
    'email' => 'email',
    'verified' => 'verified',
    'not_verified' => 'not verified',
    'TypeOfCompanyActivity' => 'Type Of Company Activity',
    'view' => 'view',
    'Recycable' => 'Recycable',
    'ProducerFamily' => 'Producer Family',
    'Item' => 'Item',
    'image' => 'image',
    'images' => 'images',
    'price' => 'price',
    'minimum_amount' => 'minimum amount',
    'maximum_amount' => 'maximum amount',
    'likes' => 'likes',
    'description_en' => 'description English',
    'description_ar' => 'description Arabic',
    'description' => 'description',
    'all' => 'all',
    'search' => 'search',
    'ProducerFamilyProduct' => 'Producer Family Product',
    'family' => 'family',
    'English' => 'English',
    'Arabic' => 'Arabic',
    'Offer' => 'Offer',
    'old_price' => 'old price',
    'new_price' => 'new price',
    'amount' => 'amount',
    'Recycling' => 'Recycling',
    'RecyclingCategory' => 'Recycling Category',
    'RecycablesNews' => 'Recycables News',
    'title' => 'title',
    'body' => 'body',
    'title_ar' => 'title Arabic',
    'title_en' => 'title English',
    'body_ar' => 'body Arabic',
    'body_en' => 'body English',
    'RecycablesWhenfullRequests' => 'Recycables When full Requests',
    'WhenfullRequests' => 'When full Requests',
    'request_code' => 'request code',
    'recycable_name' => 'recycable name',
    'recycable_id' => 'recycable id',
    'comment' => 'comment',
    'Comment' => 'Comment',
    'click for make it done' => 'click for make it done',
    'recycable_phone' => 'recycable phone',
    'request' => 'request',
    'AuctionRequest' => 'Auction Request',
    'required_quantity' => 'required quantity',
    'price_offer' => 'price offer',
    'offers order by price' => 'offers order by price',
    'no offers' => 'no offers',
    'buyer_id' => 'buyer id',
    'payment_method' => 'payment method',
    'is_paid' => 'is paid',
    'is_delivered' => 'is delivered',
    'item_id' => 'item id',
    'item_name' => 'item name',
    'quantity' => 'quantity',
    'single_price' => 'single price',
    'total_price' => 'total price',
    'offer_id' => 'offer id',
    'offer_name' => 'offer name',
    'items' => 'items',
    'Recipt' => 'Recipt',
    'Offers' => 'Offers',
    'recycables_whenfull_requests' => 'recycables whenfull requests',
    'Whenfull' => 'When full',
    'Whenfull_is_done' => 'Whenfull is done',
    'Whenfull_not_done' => 'Whenfull not done',
    'producer_family_products' => 'producer family products',
    'auction_requests' => 'auction requests',
    'All Company' => 'All Company',
    'All Buyer' => 'All Buyer',
    'All Recycable' => 'All Recycable',
    'All ProducerFamily' => 'All ProducerFamily',
    'All Item' => 'All Item',
    'All Buyer' => 'All Buyer',
    'All ProducerFamilyProduct' => 'All ProducerFamilyProduct',
    'All AuctionRequest' => 'All AuctionRequest',
    'All Offer' => 'All Offer',
    'edit' => 'edit',
    'delete' => 'delete',
    'DashBoard' => 'DashBoard',
    'main' => 'main',
    'accaptance' => 'accaptance',
    'make done' => 'make done',
    'add' => 'add',
    'permissions' => 'permissions',
    'Create Role' => 'Create Role',
    'Role' => 'Role',
    'update' => 'update',
    'place' => 'place',
    'Roles' => 'Roles',
    'add new' => 'add new',
    'Admin' => 'Admin',
    'username' => 'username',
    'role' => 'role',
    'Full Power' => 'Full Power',
    'id' => 'id',
    'edit Admin' => 'edit Admin',
    'password' => 'password',
    'password again' => 'password again',
    'create Admin' => 'create Admin',
    'DashBoard' => 'DashBoard',
    //------flag------
    'Category' => 'Category',
    'Welcome to HDC Save More With Our Deals' => 'Welcome to HDC Save More With Our Deals',
    'Login' => 'Login',
    'Register' => 'Register',
    'search store' => 'search store',
    'Home' => 'Home',
    'About Us' => 'About Us',
    'How it work' => 'How it work',
    'Outlets' => 'Outlets',
    'Vouchers' => 'Vouchers',
    'Contact us' => 'Contact us',
    'Language' => 'Language',
    'Order Now' => 'Order Now',
    'Latest Coupon Codes' => 'Latest Coupon Codes',
    'Visit all Coupons' => 'Visit all Coupons',
    'More Than 3000+ Stores In One Place!' => '',
    'Search your favourite store get many deals' => '',
    'Visit all stores' => 'Visit all stores',
    'HDC Featured Category' => 'HDC Featured CategoryHDC Featured Category',
    'Explore the popular categories in HDC' => 'Explore the popular categories in HDC',
    'view Coupon' => 'view Coupon',
    'Visit all categoires' => 'Visit all categoires',
    'Get More voucher Save More Money' => 'Get More voucher Save More Money',
    'Order Now' => 'Order Now',
    'Today’s Best Offers And Coupons' => 'Today’s Best Offers And Coupons',
    'First Name' => 'First Name',
    'Last Name' => 'Last Name',
    'Email Address' => 'Email Address',
    'Phone' => 'Phone',
    'Become an HDC Card® member and start saving today!' => 'Become an HDC Card® member and start saving today!',
    'Submit Details' => 'Submit Details',
    'Who We are' => 'Who We are',
    'About Our Company' => 'About Our Company',
    'Contact Us' => 'Contact Us',
    'Feel Free To Connect With Us' => 'Feel Free To Connect With Us',
    'Get in touch with our company team' => 'Get in touch with our company team',
    'Name' => 'Name',
    'Subject' => 'Subject',
    'Your Message' => 'Your Message',
    'Send Message' => 'Send Message',
    'Category' => 'Category',
    'view Coupon' => 'view Coupon',
    'Coupons' => 'Coupons',
    'Today’s Best Offers And Coupons' => 'Today’s Best Offers And Coupons',
    'city' => 'city',
    'district' => 'district',
    'Stores' => 'Stores',
    'search Stores' => 'search Stores',
    'search' => 'search',
    'OFF' => 'OFF',
    'you message is send' => 'you message is send',
    'you order is done we will call you' => 'you order is done we will call you',
    'Quick Links' => 'Quick Links',
    'Categories' => 'Categories',
    'Follow us' => 'Follow us',
    '2018 All rights reserved' => '2018 All rights reserved',
    'Be a Merchant' => 'Be a Merchant',
    'Apply as Merchant' => 'Apply as Merchant',
    'Apply now' => 'Apply now',
    'trade_name' => 'trade name',
    'Owner/Company name' => 'Owner/Company name',
    'How many branches' => 'How many branches',
    'Website-Facebook page' => 'Website-Facebook page',
    'Contact persone' => 'Contact persone',
    'please complete the form' => 'please complete the form',
    'please enter a vaild email' => 'please enter a vaild email',
    'Contact HDC' => 'Contact HDC',
    'Career' => 'Career',
    'Apply New Career' => 'Apply New Career',
    'Build your Career with us' => 'Build your Career with us',
    'address' => 'address',
    'job' => 'job',
    'MerchantOrder' => 'MerchantOrder',
    'Career' => 'Career',
    'Our Location' => ' Our Location ',
    'Call Us' => ' Call Us ',
    '' => '',
    '' => '',
    '' => '',
    '' => '',



];
