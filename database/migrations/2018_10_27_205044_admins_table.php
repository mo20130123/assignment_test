<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 225);
            $table->string('username', 225);
            $table->string('password', 225);
            $table->timestamps();
            $table->rememberToken();
        });

        DB::table('admins')->insert([
           'name' => 'zayed',
           'username' => 'admin',
           'password' => '$2y$10$D.7.ErEs51IO4.4BI4WGdOhGxbKtJD.j/WoXvZh634UpdWHbewBUG', // 222222
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
