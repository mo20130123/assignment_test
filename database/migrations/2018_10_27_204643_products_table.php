<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 225);
            $table->string('image', 225);
            $table->integer('Quanity');
            $table->float('Price' );
            $table->timestamps();
        });

        DB::statement('ALTER TABLE `products` CHANGE `Price` `Price` FLOAT NOT NULL  '); //fix laravel bug of (float translate to float in DB)

        //--insert some test data--
        $images = ['img_1','img_2','img_3','img_4','img_5'];
        for ($i=1; $i < 51 ; $i++)
        {
              DB::table('products')->insert([
                  'name' => 'product '.$i ,
                  'image' => $images[array_rand($images,1)]  . '.jpg',  //get random image
                  'Quanity' => ( $i * 2 ),
                  'Price' => ( $i * 10 ),
                  'created_at' => date('Y-m-d H:i:s'),
                  'updated_at' => date('Y-m-d H:i:s'),
              ]);
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
