<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class ProductController extends Controller
{
         public function __construct()
         {
            $this->middleware('auth');
         }

         public function index()
         {
            return view('Admin.Product.index');
         }

         //---api----
         public function get_list(Request $request)
         {
              $search = $request->search;
              return Product::
               where(function($q)use($search){
                   if ($search)
                     $q->where('name','like','%'.$search.'%')->orWhere('id',$search) ;
               })
               ->orderBy('name',$request->order )->paginate();
         }


         public function store(Request $request)
         {
               $validator = \Validator::make($request->all(), [
                   'name' => 'required',
                   'image' => 'required',
                   'Quanity' => 'required',
                   'Price' => 'required',
               ]);
               if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }
               $Product = Product::create($request->except('_token'));
               $Product = Product::find($Product->id);
               return response()->json([
                 'status' => 'success',
                 'data' => $Product
               ]);
         }

         //--api--
         public function update(Request $request)
         {
            $validator = \Validator::make($request->all(), [
                 'id' => 'required',
                 'name' => 'required',
                 'image' => '',
                 'Quanity' => 'required',
                 'Price' => 'required',
            ]);
            if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }

            $Product = Product::findOrFail($request->id);
            $Product->update($request->except('_token'));
            $Product = Product::find($Product->id);
            return response()->json([
               'status' => 'success',
               'data' => $Product
            ]);
         }


         //--api--
         public function destroy($id)
         {
              try {
                $deleted = Product::destroy($id);
              } catch (\Exception $e) {
                return 'false';
              }
              return 'true';
         }

}
