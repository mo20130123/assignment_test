<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
   protected $fillable = [
      'name','image', 'Quanity', 'Price'
   ];

   public function getImageAttribute($value){
      return asset('images/products/'.$value);
   }

    public function setImageAttribute($value)
    {
        if($value && $value !="undefined" )
        {
            $fileName = rand(11111,99999).'.'.$value->getClientOriginalExtension(); // renameing image
            $destinationPath = public_path('images/products');
            $value->move($destinationPath, $fileName); // uploading file to given path
            $this->attributes['image'] = $fileName;
        }
    }

}
