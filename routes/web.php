<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
//----login----
Route::get('/login', 'Auth\LoginController@admin_login_form')->name('login');
Route::post('/login', 'Auth\LoginController@admin_login');

Route::get('/home',function(){
    return redirect('Product');
});

//=============================================================================
//=============================Main Admin======================================
//=============================================================================

Route::get('/', function () {
    return redirect('login');
});


//-----------------from HEAR------------------------
 
Route::group(['prefix'=>'Product', 'namespace'=>'Admin' ],function(){
      Route::get('/', 'ProductController@index');

      //---apis---
      Route::post('/list', 'ProductController@get_list');
      Route::get('/delete/{id}', 'ProductController@destroy');
      Route::post('/create', 'ProductController@store');
      Route::post('/update', 'ProductController@update');
});
